plugins {
    kotlin("jvm") version "1.3.72" apply false
    kotlin("plugin.serialization") version "1.3.72" apply false
    id("com.github.johnrengelman.shadow") version "6.0.0" apply false
}

subprojects {
    group = "me.profiluefter"
    apply(plugin = "org.jetbrains.kotlin.jvm")
    repositories {
        mavenCentral()
    }
    dependencies {
        "implementation"(kotlin("stdlib"))
        if (this@subprojects.name != "common")
            "implementation"(project(":common"))
    }
}

project(":mqtt-dsl") {
    apply(plugin = "org.jetbrains.kotlin.plugin.serialization")

    repositories {
        jcenter()
    }

    dependencies {
        "implementation"("org.eclipse.paho", "org.eclipse.paho.client.mqttv3", "1.2.4")
        "implementation"("org.jetbrains.kotlinx:kotlinx-serialization-runtime:0.20.0")
        "implementation"("org.jetbrains.kotlinx:kotlinx-serialization-protobuf:0.20.0")
    }
}

project(":mqtt-to-influx") {
    dependencies {
        "implementation"("com.influxdb:influxdb-client-java:1.9.0")
        "implementation"(project(":mqtt-dsl"))
    }
}

project(":common") {
    dependencies {
        "api"("com.google.guava:guava:29.0-jre")
        "api"("com.influxdb:influxdb-client-java:1.9.0")
        "implementation"("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
    }
}

project(":main") {
    apply(plugin = "com.github.johnrengelman.shadow")

    dependencies {
        "implementation"("com.google.guava:guava:29.0-jre")
        "implementation"("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")

        project.parent!!.subprojects.filter { it.name != "main" }.forEach {
            "implementation"(it)
        }
    }

    tasks {
        named<Jar>("jar") {
            manifest {
                attributes(mapOf(
                        "Main-Class" to "me.profiluefter.homelabMF.MainKt",
                        "Implementation-Version" to "TODO"
                ))
            }
            this.destinationDirectory
        }
        named<Jar>("shadowJar") {
            doLast {
                assert(outputs.files.files.size == 1)
                this.outputs.files.forEach {
                    it.copyTo(File("./build/executable.jar"), overwrite = true)
                }
            }
        }
    }
}

tasks {
    task<Delete>("clean") {
        delete("./build/executable.jar")
    }
}

tasks.register("executable") {
    group = "build"
    description = "Creates a executable jar"

    dependsOn(":main:shadowJar")
}