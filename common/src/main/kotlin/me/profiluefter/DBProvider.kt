package me.profiluefter

import com.influxdb.client.InfluxDBClient
import com.influxdb.client.InfluxDBClientFactory

val influx: InfluxDBClient =
        InfluxDBClientFactory.create(
                System.getProperty("influx.url", "http://127.0.0.1:8086"),
                CharArray(0),
                "homelab",
                "metrics"
        )

val mqttBroker: String = System.getProperty("mqtt.url", "tcp://127.0.0.1:1883")