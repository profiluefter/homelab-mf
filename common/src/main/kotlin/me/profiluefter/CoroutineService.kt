package me.profiluefter

import com.google.common.util.concurrent.AbstractService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch

abstract class CoroutineService : AbstractService() {
    lateinit var job: Job
    override fun doStart() {
        job = GlobalScope.launch {
            run()
        }
        notifyStarted()
    }

    abstract suspend fun run()

    override fun doStop() {
        GlobalScope.launch {
            job.cancelAndJoin()
            notifyStopped()
        }
    }
}