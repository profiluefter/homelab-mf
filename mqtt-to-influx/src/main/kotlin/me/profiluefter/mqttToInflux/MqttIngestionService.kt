package me.profiluefter.mqttToInflux

import com.influxdb.client.write.Point
import me.profiluefter.CoroutineService
import me.profiluefter.influx
import me.profiluefter.mqttBroker
import me.profiluefter.mqttDsl.MQTT.Companion.mqtt

class MqttIngestionService : CoroutineService() {
    @ExperimentalStdlibApi
    override suspend fun run() {
        mqtt(mqttBroker, "mqttToInflux") {
            on("metrics/pixel/presence") {
                influx.writeApi.writePoint(
                        Point.measurement("presence")
                                .addField("wifi", it.decodeToString())
                                .addTag("device", "pixel")
                )
            }
            on("metrics/pixel/battery") {
                influx.writeApi.writePoint(
                        Point.measurement("battery")
                                .addField("percent", it.decodeToString().toInt())
                                .addTag("device", "pixel")
                )
            }
        }
    }
}