package me.profiluefter.homelabMF

import com.google.common.util.concurrent.Service
import com.google.common.util.concurrent.ServiceManager
import me.profiluefter.homelabMF.Version.Companion.version
import me.profiluefter.mqttToInflux.MqttIngestionService
import java.time.Duration
import java.util.concurrent.TimeoutException

val services: Array<Service> = arrayOf(
    MqttIngestionService()
)

class Version {
    companion object {
        val version: String
            get() = Version::class.java.`package`.implementationVersion ?: "unknown"
    }
}

fun main() {
    println("Starting version: $version\n")

    try {
        ServiceManager(services.asIterable()).apply {
            startAsync()
            awaitHealthy(Duration.ofMinutes(1))
            reportStartup(startupTimes())
            awaitStopped()
        }
    } catch (x: TimeoutException) {
        System.err.println(x.message)
    }
}

fun reportStartup(startupTimes: Map<Service, Long>) { //TODO: ingest to influx
    println("Startup times: ")
    startupTimes.forEach {
        println("${it.key.javaClass.simpleName}: ${it.value}ms")
    }
}