package me.profiluefter.mqttDsl

import kotlinx.serialization.Serializable
import me.profiluefter.mqttDsl.MQTT.Companion.mqtt

fun main() {
    val mqtt = mqtt("tcp://10.0.0.10:1883", "test") {
        on("test") {
            println("Received array: $it")
        }

        onSerialized("test", XYZ.serializer()) {
            println("1")
            assert(it.x == "Ok, boomer")
            assert(it.y == 187)
            println("3 $it")
        }
    }

    println("Sending...")
    mqtt.send("test", XYZ.serializer(), XYZ("Ok, boomer", 187))
    println("Sent")
}

@Serializable
data class XYZ(val x: String, val y: Int)