package me.profiluefter.mqttDsl

import kotlinx.serialization.KSerializer
import kotlinx.serialization.protobuf.ProtoBuf
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

class MQTT(broker: String, clientID: String, private val username: String? = null, private val password: String? = null) {
    companion object {
        fun mqtt(broker: String, clientID: String, init: MQTT.() -> Unit): MQTT {
            return MQTT(broker, clientID).apply(MQTT::connect).apply(init)
        }
    }

    private val client = MqttClient(broker, clientID, MemoryPersistence())

    private fun connect() = client.connect(MqttConnectOptions().apply {
        isCleanSession = false
        if (this@MQTT.username != null)
            userName = this@MQTT.username
        if (this@MQTT.password != null)
            password = this@MQTT.password.toCharArray()
    })

    fun send(topic: String, message: ByteArray, qos: Int = 2, retained: Boolean = false) =
            client.publish(topic, message, qos, retained)

    fun <K> send(topic: String, serializer: KSerializer<K>, value: K, qos: Int = 2, retained: Boolean = false) =
            send(topic, ProtoBuf(encodeDefaults = false).dump(serializer, value), qos, retained)

    fun on(topic: String, handler: (ByteArray) -> Unit) =
            client.subscribe(topic) { _, message -> handler(message.payload) }

    fun <T> onSerialized(topic: String, serializer: KSerializer<T>, handler: (T) -> Unit) {
        on(topic) {
            handler(ProtoBuf.load(serializer, it))
        }
    }
}